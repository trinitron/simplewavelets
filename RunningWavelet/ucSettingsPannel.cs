﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using RunningWavelet.Properties;
using RunningWavelet.WaveletController;
using WaveletApplication;

namespace RunningWavelet
{
    public partial class ucSettingsPannel : UserControl, IConnetionSettings, IWaveletSettings
    {
        private WaveletLoader _waveletLoader;

        public ucSettingsPannel()
        {
            InitializeComponent();
            InitializeSources();
            btnApply.Click += OnApplySettingsEvent;
            btnConnect.Click += OnWagoConnectEvent;
            btnDisconnect.Click += OnWagoConnectEvent;
            btnCheck.Click += OnWagoCheckRegistersEvent;
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public IList<string> WaveletListSource { get; set; }
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public IList<string> ThresholdSource { get; set; }

        public event EventHandler ApplySettingsEvent;
        public event EventHandler WagoConnectEvent;
        public event EventHandler WagoDisconnectEvent;
        public event EventHandler WagoCheckRegisters;

        private void InitializeSources()
        {
            _waveletLoader = new WaveletLoader();
            _waveletLoader.LoadWaveletSettings();
            WaveletListSource = _waveletLoader.GetWaveletsNames;
            ThresholdSource = Enum.GetNames(typeof(ThresholdTypes));
        }

        #region Interface WAGO
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int WagoPollCycle
        {
            get { return Settings.Default.Wago_PollCycle; }
            set
            {
                int pvalue;
                int.TryParse(value.ToString(), out pvalue);
                if (pvalue != 0)
                {
                    Settings.Default.Wago_PollCycle = pvalue;
                }
            }
        }
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public short WagoRegisterStart
        {
            get { return Settings.Default.Wago_RegisterStart; }
            set
            {
                short pvalue;
                short.TryParse(value.ToString(), out pvalue);
                if (pvalue != 0)
                {
                    Settings.Default.Wago_RegisterStart = pvalue;
                }
            }
        }
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public short WagoRegisterCount
        {
            get { return Settings.Default.Wago_RegisterCount; }
            set
            {
                short pvalue;
                short.TryParse(value.ToString(), out pvalue);
                if (pvalue != 0)
                {
                    Settings.Default.Wago_RegisterCount = pvalue;
                }
            }
        }
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public string WagoAddressIp
        {
            get { return Settings.Default.Wago_AddressIP; }
            set
            {
                if (value != null)
                    Settings.Default.Wago_AddressIP = value;
            }
        }
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int WagoPort
        {
            get { return Settings.Default.Wago_Port; }
            set
            {
                int pvalue;
                int.TryParse(value.ToString(), out pvalue);
                if (pvalue != 0)
                {
                    Settings.Default.Wago_Port = pvalue;
                }
            }
        }
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int WagoTimeOut
        {
            get { return Settings.Default.Wago_TimeOut; }
            set
            {
                int pvalue;
                int.TryParse(value.ToString(), out pvalue);
                if (pvalue != 0)
                {
                    Settings.Default.Wago_TimeOut = pvalue;
                }
            }
        }

        #endregion

        #region Events

        protected virtual void OnApplySettingsEvent(object sender, EventArgs eventArgs)
        {
            var handler = ApplySettingsEvent;
            if (handler != null) handler(sender, eventArgs);
        }

        protected virtual void OnWagoConnectEvent(object sender, EventArgs eventArgs)
        {
            var handler = WagoConnectEvent;
            if (handler != null) handler(this, EventArgs.Empty);
        }

        protected virtual void OnWagoDisconnectEvent(object sender, EventArgs eventArgs)
        {
            var handler = WagoDisconnectEvent;
            if (handler != null) handler(this, EventArgs.Empty);
        }

        protected virtual void OnWagoCheckRegistersEvent(object sender, EventArgs eventArgs)
        {
            var handler = WagoCheckRegisters;
            if (handler != null) handler(this, EventArgs.Empty);
        }

        #endregion

        #region Interface Wavelet
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public WaveletSettings ActiveWavelet
        {
            get { return _waveletLoader.GetWaveletByName(cbxWaveletType.SelectedText) ?? _waveletLoader.GetDefaultWavelet(); }
            set { value = null; }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int PlotBufferLength
        {
            get
            {
                int pvalue;
                int.TryParse(txtPlotBufferLength.Text, out pvalue);
                return pvalue;
            }
            set { txtPlotBufferLength.Text = value.ToString(); }
        }
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int DecompilationDepth
        {
            get { return (int)numDecompositionLevel.Value; }
            set { numDecompositionLevel.Value = value; }
        }
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public float ThresholdValue
        {
            get
            {
                float pvalue;
                float.TryParse(txtThresholdValue.Text, out pvalue);
                return pvalue;
            }
            set { txtThresholdValue.Text = value.ToString(); }
        }

        #endregion
    }
    public enum ThresholdTypes
    {
        Твердый = 0,
        Мягкий = 1
    }
}
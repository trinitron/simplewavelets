﻿using System.Collections.Generic;
using OxyPlot;
using RunningWavelet.WaveletController;

namespace RunningWavelet
{
    public interface IUserFields
    {
        IConnetionSettings ConnectionSettings { get; set; }
        IWaveletSettings WaveletSettings { get; set; }
        IEnumerable<DataPoint> Chart1Data { get; set; }
        IEnumerable<DataPoint> Chart2Data { get; set; }
        string GeneratorName { get; set; }
        string StatusString { get; set; }
        void SetController(RunningWaveletController controller);
    }
}
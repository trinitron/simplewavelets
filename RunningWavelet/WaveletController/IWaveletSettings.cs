﻿using WaveletApplication;

namespace RunningWavelet.WaveletController
{
    public interface IWaveletSettings
    {
        WaveletSettings ActiveWavelet { get; set; }
        int PlotBufferLength { get; set; }
        int DecompilationDepth { get; set; }
        float ThresholdValue { get; set; }


    }
}
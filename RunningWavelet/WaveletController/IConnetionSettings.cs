﻿namespace RunningWavelet.WaveletController
{
    public interface IConnetionSettings
    {
        string WagoAddressIp { get; set; }
        int WagoPort { get; set; }
        int WagoTimeOut { get; set; }
        int WagoPollCycle { get; set; }
        short WagoRegisterStart { get; set; }
        short WagoRegisterCount { get; set; }
    }
}
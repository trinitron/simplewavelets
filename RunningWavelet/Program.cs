﻿using System;
using RunningWavelet.WaveletController;

namespace RunningWavelet
{
    internal static class Program
    {
        /// <summary>
        ///     The main entry point for the application.
        /// </summary>
        [STAThread]
        private static void Main()
        {
            //http://www.codeproject.com/Articles/383153/The-Model-View-Controller-MVC-Pattern-with-Csharp
            //Application.EnableVisualStyles();
            //Application.SetCompatibleTextRenderingDefault(false);
            //Application.Run(new Form1());

            //UsersView view = new UsersView();
            var view = new Form1();
            view.Visible = false;
            var controller = new RunningWaveletController(view);
            view.ShowDialog();
        }
    }
}
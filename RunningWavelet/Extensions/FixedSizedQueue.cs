﻿using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;

namespace RunningWavelet.Extensions
{
    public class FixedSizedQueue<T>:IEnumerable<T>
    {
        ConcurrentQueue<T> q = new ConcurrentQueue<T>();

        public int Limit { get; set; }
        public void Enqueue(T obj)
        {
            q.Enqueue(obj);
            lock (this)
            {
                T overflow;
                while (q.Count > Limit && q.TryDequeue(out overflow)) ;
            }
        }

        public IEnumerator<T> GetEnumerator()
        {
            return q.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable) q).GetEnumerator();
        }
    }
}

﻿using System;

namespace RunningWavelet.DataGenerators
{
    public class SinGenerator : GenerableBase
    {
        public double SignalAmplitude { get; set; }
        public int NoiseAmplitude { get; set; }
        public int SignalFrequency { get; set; }

        protected internal override double GenerateValue()
        {
            var time = DateTime.Now;
            var rand = new Random();
            var dt = time.Second - 1 + (double)time.Millisecond / 1000;
            return SignalAmplitude*Math.Sin(dt*SignalFrequency/6) + rand.Next(NoiseAmplitude*(-1), NoiseAmplitude);
        }
    }
}
﻿namespace RunningWavelet.DataGenerators
{
    public abstract class GenerableBase
    {
        protected internal abstract double GenerateValue();
    }
}